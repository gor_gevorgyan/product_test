
const url        = 'http://search.unbxd.io/fb853e3332f2645fac9d71dc63e09ec1/demo-unbxd700181503576558/search?&q=* ';
const url_search = 'http://search.unbxd.io/fb853e3332f2645fac9d71dc63e09ec1/demo-unbxd700181503576558/search?&q='; 


function loadProduct() {
    let xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        let products = JSON.parse(this.responseText);
        if(products.response.products.length>0){
          createProduct(products);
        }
      }
    };
    xhttp.open("GET", url, true);
    xhttp.send();
  }

  loadProduct();



  function searchProduct(search_val) {
    let xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        let products = JSON.parse(this.responseText);
        if(products.response.products.length>0){
          createProduct(products);
        }
      }
    };
    url_search_add=url_search+search_val;
    xhttp.open("GET", url_search_add, true);
    xhttp.send();
  }


  document.getElementById('searchBtn').addEventListener("click", search);
  document.getElementById('search').addEventListener("keypress", (e)=>{
    if (e.keyCode == 13) {
      search();
    }
  });


  function search(){
   let search_val= document.getElementById('search').value;
   if(search_val==''){
    search_val='*';
    }
    searchProduct(search_val)
  }



  function createProduct(data){
    let  html_products='';
    let products_arr =data.response.products;

    document.getElementById('result').innerHTML=data.response.numberOfProducts;
    document.getElementById('mob_result').innerHTML=data.response.numberOfProducts;
     console.log(data);
      for(let i=0; i<products_arr.length; i++){

       let price_arr=products_arr[i].displayPrice.split(".");
       let val_p=price_arr[0][0];
       let price=price_arr[0].slice(1);
        html_products+=`<div class="div_product">
                           <div class="div_img">
                              <img src="${products_arr[i].productImage}" class="product_img">
                            </div>
                           <div class="info_content">
                             <div class="title_div">
                               <h2 class="title">${products_arr[i].title}</h2>
                             </div>
                             <div class="star_div">
                                <img src="img/star.svg" class="star_img">
                                <img src="img/star.svg" class="star_img">
                                <img src="img/star.svg" class="star_img">
                                <img src="img/star_g.svg" class="star_img">
                                <img src="img/star_g.svg" class="star_img">
                                <h2 class="star_txt">(134)</h2>
                             </div>
                             <div class="offer_div">
                               <img src="img/delivery-truck.svg" class="delivery_truck">
                               <span class="offer">shipping offer</span>
                             </div>
                             <div class="price_div">
                             <span class="price"><sup class="sup">${val_p}</sup>${price}<sup class="sup">${price_arr[1]}</sup></span>
                             </div>
                             <div class="length_div">
                               <h2 class="length"> ${products_arr[i].color.length}  color / ${products_arr[i].size.length} size</h2>
                               <img src="img/heart.svg" class="heart_img">
                             </div>
                           </div>
                        </div>`
     
      }



    document.getElementById('products').innerHTML=html_products;


  }


  // for mobile

  document.getElementById('filter').addEventListener('click',()=>{

    document.getElementById('open').style.display = "block";
    document.querySelector('.div_filter_1').style.display = "none";
    document.querySelector('.content_main').style.display = "none";
    document.querySelector('.content').style.display = "block";
    
  });


  document.getElementById('filter_close').addEventListener('click',()=>{

    document.getElementById('open').style.display = "none";
    document.querySelector('.div_filter_1').style.display = "block";
    document.querySelector('.content_main').style.display = "block"; 
    document.querySelector('.content').style.display = "none";
    
  });